﻿using System.Configuration;

namespace NoteAMoonKey.Resources.ConfigManager.ConfigSections
{
    public class CalculationSettingsConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("workdingHoursPerDay", DefaultValue = "8", IsRequired = true, IsKey = true)]
        public int WorkingHoursPerDay => (int)this["workdingHoursPerDay"];

        [ConfigurationProperty("expectedCreatingOfCodeThreshold", DefaultValue = "0.8", IsRequired = true, IsKey = true)]
        public decimal ExpectedCreatingOfCodeThreshold => (decimal)this["expectedCreatingOfCodeThreshold"];

        [ConfigurationProperty("expectedCreatingOfDocsThreshold", DefaultValue = "0.1", IsRequired = true, IsKey = true)]
        public decimal ExpectedCreatingOfDocsThreshold => (decimal)this["expectedCreatingOfDocsThreshold"];

        [ConfigurationProperty("decimals", DefaultValue = "0", IsRequired = true, IsKey = true)]
        public int Decimals => (int)this["decimals"];

        [ConfigurationProperty("dispersion", DefaultValue = "0.7", IsRequired = true, IsKey = true)]
        public decimal Dispersion => (decimal)this["dispersion"];
    }
}

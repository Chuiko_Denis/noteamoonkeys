﻿using System;
using System.Configuration;
using NoteAMoonKey.Resources.ConfigManager.Extensions;
using Serilog;

namespace NoteAMoonKey.Resources.ConfigManager.ConfigSections
{
    public class AppSettingsConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("targetSiteEntryPoint", DefaultValue = null, IsRequired = false)]
        public Uri TargetSiteEntryPoint => (Uri) this["targetSiteEntryPoint"];

        [ConfigurationProperty("appDataFolder", DefaultValue = "%appdata%/Denys Chuiko/Note A Moon Key",
             IsRequired = false)]
        public string AppDataFolder
        {
            get
            {
                var result = ((string)this["appDataFolder"]).ReplaseEnvironmentVariables();

                if (!Uri.IsWellFormedUriString(result, UriKind.Absolute))
                {
                    Log.Warning($"Config parameter [{nameof(AppDataFolder)}] describes invalid path: {result}.");
                    result = "%appdata%/Denys Chuiko/Note A Moon Key"
                        .ReplaseEnvironmentVariables();
                }

                return result;
            }
        }

        [ConfigurationProperty("logRelativePath", DefaultValue = "Logs/log-{Date}.txt", IsRequired = false)]
        public string LogRelativePath => ((string) this["logRelativePath"]).ReplaceAvailableAliaces();

        [ConfigurationProperty("dateFormatForReport", DefaultValue = "dd/MM/yyyy", IsRequired = false)]
        public string DateFormat => (string) this["dateFormatForReport"];
    }
}

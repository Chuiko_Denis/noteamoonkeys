﻿using System.Configuration;
using System.Linq;

namespace NoteAMoonKey.Resources.ConfigManager.ConfigSections
{
    internal class CssSelectorsConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("", IsKey = true, IsDefaultCollection = true)]
        public SelectorsCollection Items
        {
            get { return (SelectorsCollection) this[""]; }
            set { this[""] = value; }
        }

        internal string this[CssSelectorsKeys key] => Items[key].Value;
    }

    internal class SelectorsCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new CssSelector();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((CssSelector)element).Key;
        }

        internal CssSelector this[CssSelectorsKeys key] => this.Cast<CssSelector>().Single(e => e.Key == key.ToString());
    }

    internal class CssSelector : ConfigurationElement
    {
        [ConfigurationProperty("key", IsKey = true, IsRequired = true)]
        public string Key => (string)this["key"];

        [ConfigurationProperty("value", IsKey = true, IsRequired = true)]
        public string Value => (string)this["value"];
    }

    internal enum CssSelectorsKeys
    {
        ReportPageIdentificator,
        NewWorkDayRegistrationPageIdentificator,
        NewWorkDayRegistrationPageOpenLink,
        NewWorkDayDateInputField,
        NewWorkDayCreationOfCodeHoursInputField,
        NewWorkDayCreationOfDocumentationHoursInputField,
        NewWorkDayOtherTasksHoursInputField,
        NewWorkDayCancelButton,
        NewWorkDaySaveButton
    }
}

﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace NoteAMoonKey.Resources.ConfigManager.Extensions
{
    internal static class StringExtensions
    {
        internal static string ReplaseEnvironmentVariables(this string source)
        {
            var environmentVariables = Regex.Matches(source, @"%\w*%", RegexOptions.IgnoreCase)
                .Cast<Match>().Select(e => e.Value).ToList();

            environmentVariables.ForEach(e => source = source.Replace(e, Environment.ExpandEnvironmentVariables(e)));

            return source;
        }

        internal static string ReplaceAvailableAliaces(this string source)
        {
            return source.Replace("{Date}", DateTime.Now.ToString("yyyy_MM_dd"));
        }
    }
}

﻿using System.Configuration;
using NoteAMoonKey.Resources.ConfigManager.ConfigSections;

namespace NoteAMoonKey.Resources.ConfigManager
{
    internal class ConfigManager : IConfigManager
    {
        public AppSettingsConfigSection MainSettings 
            => (AppSettingsConfigSection) ConfigurationManager.GetSection("coreParametersGroup/coreParameters");

        public CalculationSettingsConfigSection CalculationSettings
            => (CalculationSettingsConfigSection) ConfigurationManager.GetSection("coreParametersGroup/calculationSettings");

        public CssSelectorsConfigSection CssSelectors 
            => (CssSelectorsConfigSection) ConfigurationManager.GetSection("coreParametersGroup/cssSelectors");
    }
}

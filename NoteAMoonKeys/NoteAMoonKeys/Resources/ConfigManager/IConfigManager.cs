﻿using NoteAMoonKey.Resources.ConfigManager.ConfigSections;

namespace NoteAMoonKey.Resources.ConfigManager
{
    internal interface IConfigManager
    {
        AppSettingsConfigSection MainSettings { get; }

        CalculationSettingsConfigSection CalculationSettings { get; }

        CssSelectorsConfigSection CssSelectors { get; }
    }
}

﻿using mshtml;
using NoteAMoonKey.Engines.WebPageSurfingEngine.SurfingEngineParts;

namespace NoteAMoonKey.Engines.Extensions
{
    internal static class HTMLDocumentClassExtensions
    {
        public static DocumentProcessor GetProcessor(this HTMLDocumentClass document)
        {
            return new DocumentProcessor(document);
        }
    }
}

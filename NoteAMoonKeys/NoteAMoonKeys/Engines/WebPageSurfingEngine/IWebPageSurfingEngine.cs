﻿using System.Threading.Tasks;
using NoteAMoonKey.Models;

namespace NoteAMoonKey.Engines.WebPageSurfingEngine
{
    public interface IWebPageSurfingEngine
    {
        Task AddWorkedDatesToTheSystem(ProcessingData processingData);
    }
}

﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using mshtml;
using Newtonsoft.Json;
using NoteAMoonKey.Engines.Extensions;
using NoteAMoonKey.Models;
using NoteAMoonKey.Resources.ConfigManager;
using NoteAMoonKey.Resources.ConfigManager.ConfigSections;
using Serilog;
using SimpleInjector;

namespace NoteAMoonKey.Engines.WebPageSurfingEngine.SurfingEngineParts
{
    internal class WebBrowserProcessor
    {
        private Container _container;
        private WebBrowser _webBrowserControl;
        private CssSelectorsConfigSection _selectors;
        private AppSettingsConfigSection _appSettings;

        public WebBrowserProcessor(Container container, WebBrowser webBrowserControl, IConfigManager configManger)
        {
            _container = container;
            _webBrowserControl = webBrowserControl;
            _selectors = configManger.CssSelectors;
            _appSettings = configManger.MainSettings;
        }

        private HTMLDocumentClass Document
        {
            get
            {
                HTMLDocumentClass document = null;
                document = (HTMLDocumentClass)_webBrowserControl.Document;
                if (document == null)
                    throw new Exception("Page is not loaded yet");

                return document;
            }
        }

        internal async Task StartWorkFrom(Uri url)
        {
            Log.Information(Resources.Notifications.WebSerfingHasBeenStarted);

            _webBrowserControl.Navigate(url);
        }

        internal async Task CheckTheLandPageShouldBeReportingPage(bool isTheFirstPageOpening = false)
        {
            var reportingPageName = "[Time reporting]";
            
            Log.Information(Resources.Notifications.WaitingForAnchorOnPage, reportingPageName);
            if(isTheFirstPageOpening)
                Log.Information(Resources.Notifications.ItCanTakeSomeTimeForVeryFirstPage);

            var selector = _selectors[CssSelectorsKeys.ReportPageIdentificator];
            await _container.GetInstance<Trier>()
                .InvokeAsync(() =>
                    Document.GetProcessor().CheckIfElementContains(selector, "Time Reporting")
                    );

            
            Log.Information(Resources.Notifications.ExpectedPageIsLoaded, reportingPageName);
        }

        internal async Task OpenNewWorkDayRegistrationPage()
        {
            var newWorkDayRegistrationPageName = "[Registrate new work day]";

            Log.Information(Resources.Notifications.CallingThePage, newWorkDayRegistrationPageName);

            var selector = _selectors[CssSelectorsKeys.NewWorkDayRegistrationPageOpenLink];
            await _container.GetInstance<Trier>().InvokeAsync(() => 
                Document.GetProcessor().ClickOn(selector)
            );
        }

        internal async Task CheckTheLandPageShouldBeRegistrateNewWorkDay()
        {
            var reportingPageName = "[Registrate new work day]";

            Log.Information(Resources.Notifications.WaitingForAnchorOnPage, reportingPageName);

            var selector = _selectors[CssSelectorsKeys.NewWorkDayRegistrationPageIdentificator];
            await _container.GetInstance<Trier>()
                .InvokeAsync(() =>
                    Document.GetProcessor().CheckIfUniqueElementExists(selector)
                    );


            Log.Information(Resources.Notifications.ExpectedPageIsLoaded, reportingPageName);
        }

        internal async Task SetNewWorkDayData(NewWorkDayInfo newWorkDay)
        {
            Log.Information(Resources.Notifications.InsertingDataForNewWorkDay, JsonConvert.SerializeObject(newWorkDay));

            await SetDayAsWorked(newWorkDay.Day);
            await SetCreationOfCodeHours(newWorkDay.CreationOfCodeHours);
            await SetCreationOfDocumentHours(newWorkDay.CreationOfDocumentHours);
            await SetOtherTasksHours(newWorkDay.OtherTasksHours);
        }

        internal async Task SetDayAsWorked(DateTime date)
        {
            Log.Debug(string.Format(Resources.Notifications.InputingDataToInput, date.ToString("dd/MM/yyyy"), "Date"));

            var selector = _selectors[CssSelectorsKeys.NewWorkDayDateInputField];
            var dateValue = date.ToString(_appSettings.DateFormat);
            await _container.GetInstance<Trier>()
                .InvokeAsync(() => Document.GetProcessor().SetValueForSingle(selector, dateValue));

            Log.Debug(Resources.Notifications.InteractionCompleteSuccessfully);
        }

        internal async Task SetCreationOfCodeHours(decimal creationOfCodeHours)
        {
            Log.Debug(string.Format(Resources.Notifications.InputingDataToInput, creationOfCodeHours, "Creation of code hours"));

            var selector = _selectors[CssSelectorsKeys.NewWorkDayCreationOfCodeHoursInputField];
            await _container.GetInstance<Trier>()
                .InvokeAsync(() =>
                    Document.GetProcessor().SetValueForSingle(selector, creationOfCodeHours.ToString()));

            Log.Debug(Resources.Notifications.InteractionCompleteSuccessfully);
        }

        internal async Task SetCreationOfDocumentHours(decimal creationOfDocumentHours)
        {
            Log.Debug(string.Format(Resources.Notifications.InputingDataToInput, creationOfDocumentHours, "Creation of document hours"));

            var selector = _selectors[CssSelectorsKeys.NewWorkDayCreationOfDocumentationHoursInputField];
            await _container.GetInstance<Trier>().InvokeAsync(() =>
                Document.GetProcessor().SetValueForSingle(selector, creationOfDocumentHours.ToString())
            );

            Log.Debug(Resources.Notifications.InteractionCompleteSuccessfully);
        }

        internal async Task SetOtherTasksHours(decimal otherTasksHours)
        {
            Log.Debug(string.Format(Resources.Notifications.InputingDataToInput, otherTasksHours, "Other tasks hours"));

            var selector = _selectors[CssSelectorsKeys.NewWorkDayOtherTasksHoursInputField];
            await _container.GetInstance<Trier>().InvokeAsync(() =>
                Document.GetProcessor().SetValueForSingle(selector, otherTasksHours.ToString())

            );

            Log.Debug(Resources.Notifications.InteractionCompleteSuccessfully);
        }

        internal async Task ClickCancelButton()
        {
            var selector = _selectors[CssSelectorsKeys.NewWorkDayCancelButton];
            await _container.GetInstance<Trier>().InvokeAsync(() =>
                Document.GetProcessor().ClickOn(selector)
            );
        }

        internal async Task ClickSaveButton()
        {
            Log.Debug(Resources.Notifications.InteractingWith, "Save Button");

            var selector = _selectors[CssSelectorsKeys.NewWorkDaySaveButton];
            await _container.GetInstance<Trier>().InvokeAsync(() =>
                Document.GetProcessor().ClickOn(selector)
            );

            Log.Debug(Resources.Notifications.InteractionCompleteSuccessfully);
        }

        internal async Task SaflyWaitForPageAjaxComplete()
        {
            await Task.Run(() => Thread.Sleep(300));
        }

        #region testing (work with QuickEdit-page is a hard task)

        internal async Task OpenListSection()
        {
            var selector = @"div[id $=ribbonrow] div#ribbonBox div#RibbonContainer div.ms-cui-ribbonTopBars ul.ms-cui-tts li.ms-cui-cg li.ms-cui-ct-last a";
            await _container.GetInstance<Trier>().InvokeAsync(() =>
                Document.GetProcessor().ClickOn(selector)
            );
        }

        internal async Task OpenQuickEditPage()
        {
            var selector = @"div.ms-cui-tabContainer li[id='Ribbon.List.ViewFormat'] span.ms-cui-section a[aria-describedby='Ribbon.List.ViewFormat.Datasheet_ToolTip']";
            await _container.GetInstance<Trier>().InvokeAsync(() =>
                Document.GetProcessor().ClickOn(selector)
            );
        }

        #endregion
    }
}

﻿using System.Linq;
using CsQuery;
using mshtml;
using NoteAMoonKey.Exceptions;

namespace NoteAMoonKey.Engines.WebPageSurfingEngine.SurfingEngineParts
{
    internal class DocumentProcessor
    {
        private HTMLDocumentClass _document;
        private CQ _dom;

        public DocumentProcessor(HTMLDocumentClass document) {
            _document = document;
            _dom = _document.documentElement.outerHTML;
        }

        #region JavaScript interaction

        public void SetValueForSingle(string selector, string value)
        {
            CheckIfUniqueElementExists(selector);

            var script = $"document.querySelector(\"{selector}\").setAttribute('value', '{value}')";
            _ExecuteScript(script);
        }

        internal void ClickOn(string selector)
        {
            CheckIfUniqueElementExists(selector);

            var script = $"document.querySelector(\"{selector}\").click();";
            _ExecuteScript(script);
        }

        #endregion

        internal void CheckIfElementContains(string selector, string expectedText)
        {
            var element = _GetSingle(selector);
            var currentText = element.InnerText;
            if(!currentText.Equals(expectedText))
                throw new NotExpectedContentException(selector, expectedText, currentText);
        }

        internal void CheckIfUniqueElementExists(string selector)
        {
            _GetSingle(selector);
        }

        private void _ExecuteScript(string script)
        {
            var htmlWindowClass = (HTMLWindow2Class)_document.Script;
            var result = htmlWindowClass.execScript(script);
        }
        
        private IDomElement _GetSingle(string selector)
        {
            var elements = _dom.Find(selector).Selection.Cast<IDomElement>();
            if (elements.Count() > 1)
                throw new HtmlElementNotTheOnlyException(selector, elements, _document.documentElement.outerHTML);
            else if (elements.Count() < 1)
                throw new HtmlElementNotFoundException(selector, _document.documentElement.outerHTML);

            return elements.First();
        }

        private void ApplyChanges()
        {
            _document.body.outerHTML = _dom["body"].RenderSelection();
        }
    }
}

﻿using System.Threading.Tasks;
using NoteAMoonKey.Engines.WebPageSurfingEngine.SurfingEngineParts;
using NoteAMoonKey.Models;
using NoteAMoonKey.Resources.ConfigManager;
using SimpleInjector;

namespace NoteAMoonKey.Engines.WebPageSurfingEngine
{
    internal class WebPageSurfingEngine : IWebPageSurfingEngine
    {
        private readonly Container _container;
        private readonly WebBrowserProcessor _webBrowserProcessor;
        private readonly IConfigManager _configManager;

        public WebPageSurfingEngine(
            Container container, 
            WebBrowserProcessor webBrowserProcessor,
            IConfigManager configManager)
        {
            _container = container;
            _webBrowserProcessor = webBrowserProcessor;
            _configManager = configManager;
        }

        public async Task AddWorkedDatesToTheSystem(ProcessingData processingData)
        {
            var targetSiteUrl = _configManager.MainSettings.TargetSiteEntryPoint;

            await _webBrowserProcessor.StartWorkFrom(targetSiteUrl);
            await _webBrowserProcessor.CheckTheLandPageShouldBeReportingPage(true);

            foreach (var dayStatistics in processingData.ResultCollection)
            {
                var newWorkDayInfo = new NewWorkDayInfo
                {
                    Day = dayStatistics.Day,
                    CreationOfCodeHours = dayStatistics.CreationOfCodeHours,
                    CreationOfDocumentHours = dayStatistics.CreationOfDocumentHours,
                    OtherTasksHours = dayStatistics.OtherTasksHours
                };

                await _webBrowserProcessor.OpenNewWorkDayRegistrationPage();
                await _webBrowserProcessor.CheckTheLandPageShouldBeRegistrateNewWorkDay();

                await _webBrowserProcessor.SetNewWorkDayData(newWorkDayInfo);

                await _webBrowserProcessor.SaflyWaitForPageAjaxComplete();
                //await _webBrowserProcessor.ClickCancelButton();
                await _webBrowserProcessor.ClickSaveButton();

                await _webBrowserProcessor.CheckTheLandPageShouldBeReportingPage();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Controls;
using mshtml;
using Newtonsoft.Json;
using NoteAMoonKey.Exceptions;

namespace NoteAMoonKey.Engines.Experiment
{
    class JavaScriptInjection
    {
        private HTMLDocumentClass _document;

        public JavaScriptInjection(HTMLDocumentClass document)
        {
            _document = document;
        }

        private void _GetSingle_Test(string selector)
        {
            var htmlWidowsClass = (HTMLWindow2Class)_document.Script;
            var webBrowser = App.container.GetInstance<WebBrowser>();
            var head = _document.getElementsByTagName("head").Cast<HTMLHeadElement>().First();

            var script = (IHTMLScriptElement)_document.createElement("script");
            script.text = @"function getElementsBySelector(selector){
                                var res = [];
                                var nodeList = document.querySelectorAll(selector);
                                for (var i = 0; i < nodeList.length; i++) {
                                    res.push(nodeList[i].outerHTML);
                                }
                                return JSON.stringify(res);
                            }";
            head.appendChild((IHTMLDOMNode)script);

            var result = (string)webBrowser.InvokeScript("getElementsBySelector", selector);

            var serializer = new JsonSerializer();
            using (var textReader = new StringReader(result))
            using (var reader = new JsonTextReader(textReader))
            {
                var res = (Newtonsoft.Json.Linq.JArray)serializer.Deserialize(reader);
            }
            //_TryCast(result);
            //var script = $"document.querySelectorAll(\"{selector}\")";
            //var result = htmlWidowsClass.execScript(script);
            if (result == null)
                throw new HtmlElementNotFoundException(selector, _document.documentElement.outerHTML);
        }

        private void _TryCast(object converting)
        {
            var appropriateTypes = new Dictionary<Type, object>();
            var types = Assembly.GetAssembly(typeof(mshtml.IHTMLElement)).GetTypes();
            foreach (var type in types)
            {
                try
                {
                    var converted = Convert.ChangeType(converting, type);
                    appropriateTypes.Add(type, converted);
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using CsQuery.ExtensionMethods;
using NoteAMoonKey.Models;
using NoteAMoonKey.Models.CalculaitonSettings;

namespace NoteAMoonKey.Engines.WorkingHoursCalculator
{
    internal class WorkingHoursCalculator : IWorkingHoursCalculator
    {
        private readonly Random _randomizer = new Random();

        private CalculationSettings _calculationSettings;

        public IEnumerable<NewWorkDayInfo> ProcessDates(ProcessingData processingData)
        {
            _calculationSettings = processingData.CalculationSettings;

            var workingDays = processingData.SelectedDates;

            var result = new List<NewWorkDayInfo>();
            
            workingDays.ForEach(day => result.Add(_CalculateNewDayInfo(day)));
            
            _AdjustStatistics(result);

            return result;
        }

        private void _AdjustStatistics(List<NewWorkDayInfo> result)
        {
            if (!result.Any()) return;

            var expectedCreativityThreshold = _calculationSettings.ExpectedCreatingOfCodeThreshold + _calculationSettings.ExpectedCreatingOfDocsThreshold;

            var counter = 0;
            while (result.Average(e => (e.CreationOfCodeHours + e.CreationOfDocumentHours) / e.Total) < expectedCreativityThreshold)
            {
                if (counter++ > 100)
                    throw new NotPossibleCaseException(result, _calculationSettings);

                var minDeviation = result.Min(e => Math.Abs(expectedCreativityThreshold - e.CreationOfCodeHours - e.CreationOfDocumentHours));
                var elementToReplace = result.First(el => Math.Abs(expectedCreativityThreshold - el.CreationOfCodeHours - el.CreationOfDocumentHours) == minDeviation);
                var newElement = _CalculateNewDayInfo(DateTime.Now, true);
                elementToReplace.AdjustWith(newElement);
            }
        }

        private NewWorkDayInfo _CalculateNewDayInfo(DateTime workDay, bool positiveDeviationOnly = false)
        {
            var workingHoursPerDay = _calculationSettings.WorkingHoursPerDay;
            var expectedCreatingOfCodeThreshold = _calculationSettings.ExpectedCreatingOfCodeThreshold;
            var expectedCreatingDocsThreshold = _calculationSettings.ExpectedCreatingOfDocsThreshold;

            var decimals = _calculationSettings.Decimals;

            if (expectedCreatingOfCodeThreshold == 1)
            {
                return new NewWorkDayInfo
                {
                    Day = workDay,
                    CreationOfCodeHours = workingHoursPerDay,
                    CreationOfDocumentHours = 0,
                    OtherTasksHours = 0
                };
            }

            var dayPie = (decimal)workingHoursPerDay;
            var creationOfCodePart = _GetDayPartWithDeviation(expectedCreatingOfCodeThreshold, dayPie, positiveDeviationOnly);
            dayPie -= creationOfCodePart;
            var creationOfDocsDayPart = _GetDayPartWithDeviation(expectedCreatingDocsThreshold, dayPie, positiveDeviationOnly);
            dayPie -= creationOfDocsDayPart;
            var othersPart = dayPie;
            
            return new NewWorkDayInfo
            {
                Day = workDay,
                CreationOfCodeHours = creationOfCodePart,
                CreationOfDocumentHours = creationOfDocsDayPart,
                OtherTasksHours = othersPart,
                ExpectingCreativity = (expectedCreatingOfCodeThreshold + expectedCreatingDocsThreshold)*workingHoursPerDay
            };
        }

        private decimal _GetDayPartWithDeviation(decimal expectedValue, decimal dayPieHours, bool positiveDeviationOnly = false)
        {
            var workingHoursPerDay = _calculationSettings.WorkingHoursPerDay;
            var totalValuesField = dayPieHours / workingHoursPerDay;
            var reallyExpectedValue = Math.Min(expectedValue, totalValuesField);
            var deviationAmplitude = Math.Min(totalValuesField - reallyExpectedValue, reallyExpectedValue);

            var dispersion = _calculationSettings.Dispersion;
            var deviationCoefficient = (decimal) (2 * _randomizer.NextDouble() - 1);

            var resultDeviationCoefficient = (positiveDeviationOnly
                                                     ? Math.Abs(deviationCoefficient)
                                                     : deviationCoefficient) * dispersion;

            var result = Math.Round(workingHoursPerDay*(reallyExpectedValue + deviationAmplitude * resultDeviationCoefficient), _calculationSettings.Decimals);
            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using NoteAMoonKey.Models;
using NoteAMoonKey.Models.CalculaitonSettings;

namespace NoteAMoonKey.Engines.WorkingHoursCalculator
{
    internal class NotPossibleCaseException : Exception
    {
        internal NotPossibleCaseException(IEnumerable<NewWorkDayInfo> lastProcessingCollection, CalculationSettings calculationSettins)
        {
            LastProcessingCollection = lastProcessingCollection;
            CalculationSettings = calculationSettins;
        }

        public IEnumerable<NewWorkDayInfo> LastProcessingCollection { get; set; }

        public CalculationSettings CalculationSettings { get; set; }

        public override string Message
        {
            get
            {
                var lastProcessingCollectionText = string.Join("\n", LastProcessingCollection.Select(e => e.ToString()));
                var calculationSettingsText = JsonConvert.SerializeObject(CalculationSettings);

                return $"Not possible to find the appropriate working schema for current parameters: \n" +
                       $"lastProcessingCollection: \n{lastProcessingCollectionText};\n" +
                       $"calculationSettings: {calculationSettingsText}";
            }
        }
    }
}

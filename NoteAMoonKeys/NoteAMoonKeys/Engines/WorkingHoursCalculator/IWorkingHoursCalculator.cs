﻿using System.Collections.Generic;
using NoteAMoonKey.Models;

namespace NoteAMoonKey.Engines.WorkingHoursCalculator
{
    internal interface IWorkingHoursCalculator
    {
        IEnumerable<NewWorkDayInfo> ProcessDates(ProcessingData processingData);
    }
}

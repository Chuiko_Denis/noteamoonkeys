﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using NoteAMoonKey.Exceptions;
using Serilog;

namespace NoteAMoonKey.Engines
{
    internal class Trier
    {
        private readonly Dispatcher _dispatcher;

        public TimeSpan Wait { get; set; } = TimeSpan.FromSeconds(60);

        public Trier(Dispatcher dispatcher)
        {
            _dispatcher = dispatcher;
        }

        public double WaitingProgress { get; private set; }

        public async Task InvokeAsync(Action action)
        {
            await Task.Run(() => Invoke(action));
        }

        public void Invoke(Action action)
        {
            var startedAt = DateTime.Now;
            var waitEndTime = startedAt.Add(Wait);

            for ( ; DateTime.Now < waitEndTime; )
            {
                try
                {
                    _dispatcher.Invoke(action);
                    return;
                }
                catch (HtmlElementNotTheOnlyException ex)
                {
                    Log.Fatal(ex.Message);
                    throw ex;
                }
                catch (Exception ex)
                {
                    Log.Warning(ex.Message);
                }

                Thread.Sleep(TimeSpan.FromMilliseconds(200));

                WaitingProgress = (DateTime.Now - startedAt).TotalMilliseconds / Wait.TotalMilliseconds;
            }

            throw new WaiterTimedOutException(Wait);
        }
    }
}

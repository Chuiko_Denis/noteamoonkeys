﻿using System;
using Serilog.Core;
using Serilog.Events;

namespace NoteAMoonKey.Logger
{
    class LogDisplay : ILogEventSink
    {
        public static Action<string> NotificationAction { get; set; }

        public void Emit(LogEvent logEvent)
        {
            if (logEvent.Level == LogEventLevel.Information)
            {
                NotificationAction?.Invoke(logEvent.RenderMessage());
            }
        }
    }
}

﻿using System;
using System.Text;

namespace NoteAMoonKey.Exceptions
{
    public class HtmlElementNotFoundException : Exception
    {
        public HtmlElementNotFoundException(string selector, string html)
        {
            Selector = selector;
            Html = html;
        }

        public override string Message
        {
            get
            {
                var result = new StringBuilder();
                result.Append(string.Format(Resources.Notifications.ElementWithSelectorWasNotFound, Selector));
#if TRACE
                result.Append($"\n{Html}");
#endif
                return result.ToString();
            }
        }

        public string Html { get; }

        public string Selector { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CsQuery;

namespace NoteAMoonKey.Exceptions
{
    internal class HtmlElementNotTheOnlyException : Exception
    {
        public HtmlElementNotTheOnlyException(string selector, IEnumerable<IDomElement> elementsFound, string html)
        {
            Selector = selector;
            ElementsFound = elementsFound;
            Html = html;
        }

        public override string Message
        {
            get
            {
                var result = new StringBuilder();
                result.Append(string.Format(Resources.Notifications.ThereAreMoreThanOneElementWithSelector, Selector, string.Join(", ", ElementsFound.Select(e => e.OuterHTML))));
#if TRACE
                result.Append($"\n{Html}");
#endif
                return result.ToString();
            }
        }

        public IEnumerable<IDomElement> ElementsFound { get; }

        public string Html { get; }

        public string Selector { get; }
    }
}

﻿using System;

namespace NoteAMoonKey.Exceptions
{
    internal class NotExpectedContentException : Exception
    {
        public string Selector { get; }
        public string ExpectedContent { get; set; }
        public string CurrentContent { get; set; }

        public NotExpectedContentException(string selector, string expectedContent, string currentContent)
            : base(string.Format(Resources.Notifications.ElementContainsNotExpectedText, selector, currentContent, expectedContent))
        {
            Selector = selector;
            ExpectedContent = expectedContent;
            CurrentContent = currentContent;
        }
    }
}

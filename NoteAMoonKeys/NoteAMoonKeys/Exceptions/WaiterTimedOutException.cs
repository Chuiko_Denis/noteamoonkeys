﻿using System;

namespace NoteAMoonKey.Exceptions
{
    internal class WaiterTimedOutException : Exception
    {
        public WaiterTimedOutException(TimeSpan elapsedTime)
            : base(string.Format(Resources.Notifications.WaitingHasFallen, elapsedTime.TotalSeconds))
        {
            ElapsedTime = elapsedTime;
        }

        public TimeSpan ElapsedTime { get; }
    }
}

﻿using System.IO;
using System.Windows;
using NoteAMoonKey.Engines.WorkingHoursCalculator;
using NoteAMoonKey.Logger;
using NoteAMoonKey.Resources.ConfigManager;
using Serilog;
using Serilog.Events;
using SimpleInjector;
using SimpleInjector.Lifestyles;

namespace NoteAMoonKey
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static Container container;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            
            _ConfigureIocContainer();
            _InitiateLogger();
        }

        private void _ConfigureIocContainer()
        {
            container = new Container();
            container.Options.DefaultScopedLifestyle = new ThreadScopedLifestyle();

            container.Register<IConfigManager, ConfigManager>(Lifestyle.Singleton);
            container.Register<IWorkingHoursCalculator, WorkingHoursCalculator>(Lifestyle.Transient);

            container.Verify();
        }

        private void _InitiateLogger()
        {
            var appSettings = container.GetInstance<IConfigManager>().MainSettings;

            string logPath = Path.Combine(appSettings.AppDataFolder, appSettings.LogRelativePath);

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File(logPath,
                    outputTemplate: "{Timestamp:yyyy-MM-hh HH:mm:ss.fff zzz} [{Level}] {Message}{NewLine}{Exception}")
                .WriteTo.Sink<LogDisplay>(restrictedToMinimumLevel: LogEventLevel.Information)
                .CreateLogger();
        }
    }
}

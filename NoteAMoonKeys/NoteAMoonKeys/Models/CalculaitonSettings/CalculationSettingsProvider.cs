﻿using System;
using System.Configuration;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using NoteAMoonKey.Resources.ConfigManager;
using NoteAMoonKey.Resources.ConfigManager.ConfigSections;

namespace NoteAMoonKey.Models.CalculaitonSettings
{
    internal class CalculationSettingsProvider : ICalculationSettingsProvider
    {
        private CalculationSettings _calculationSettings;

        private string _storingRelativePath = "StatisticsCalculationSettings.xml";

        private IConfigManager _configManager;

        public CalculationSettingsProvider(IConfigManager configManager)
        {
            _configManager = configManager;
        }

        public CalculationSettings GetCalculationSettings
        {
            get { return _calculationSettings ?? (_calculationSettings = _InitiateCalculationSettings()); }
        }

        private string _CalculationSettingsFullPath
            => Path.Combine(_configManager.MainSettings.AppDataFolder, _storingRelativePath);

        private CalculationSettings _InitiateCalculationSettings()
        {
            var result = File.Exists(_CalculationSettingsFullPath)
                ? GetFromStored()
                : _GetFromConfig();

            return result;
        }

        private CalculationSettings _GetFromConfig()
        {
            var result = (CalculationSettingsConfigSection)ConfigurationManager.GetSection("coreParametersGroup/calculationSettings");
            return (CalculationSettings) result;
        }

        private CalculationSettings GetFromStored()
        {
            using (var fileStream = new FileStream(_CalculationSettingsFullPath, FileMode.Open, FileAccess.Read))
            {
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    var serializer = new XmlSerializer(typeof(CalculationSettings));
                    var result = (CalculationSettings) serializer.Deserialize(streamReader);

                    return result;
                }
            }
        }

        public void SaveCalculationSettings(CalculationSettings settingsToStore)
        {
            var calculationStorageFullPath = _CalculationSettingsFullPath;
            if (File.Exists(calculationStorageFullPath))
                File.Delete(calculationStorageFullPath);

            using (var fileStream = new FileStream(calculationStorageFullPath, FileMode.CreateNew, FileAccess.Write))
            {
                using (var streamWriter = new StreamWriter(fileStream, Encoding.UTF8))
                {
                    var serializer = new XmlSerializer(typeof(CalculationSettings));
                    serializer.Serialize(streamWriter, settingsToStore);
                    streamWriter.Flush();
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using OxyPlot;

namespace NoteAMoonKey.Models.CalculaitonSettings.Converters
{
    internal class WorkDaysToCreationOfDocsHoursConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var source = (IEnumerable<NewWorkDayInfo>)value;
            return source.Select((e, i) => new DataPoint(i, (double)e.CreationOfDocumentHours));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

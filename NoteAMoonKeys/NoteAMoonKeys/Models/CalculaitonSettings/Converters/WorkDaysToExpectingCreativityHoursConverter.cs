﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using OxyPlot;

namespace NoteAMoonKey.Models.CalculaitonSettings.Converters
{
    internal class WorkDaysToExpectingCreativityHoursConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var source = (IEnumerable<NewWorkDayInfo>)value;
            var expectingCreativityAverageValue = source.Any()
                ? source.Average(e => e.ExpectingCreativity)
                : 0;
            return source.Select((e, i) => new DataPoint(i, (double)expectingCreativityAverageValue));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

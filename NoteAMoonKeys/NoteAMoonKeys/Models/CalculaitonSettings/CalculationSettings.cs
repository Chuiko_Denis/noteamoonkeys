﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using NoteAMoonKey.Resources.ConfigManager.ConfigSections;

namespace NoteAMoonKey.Models.CalculaitonSettings
{
    public class CalculationSettings : INotifyPropertyChanged
    {
        private int _workingHoursPerDay;
        public int WorkingHoursPerDay
        {
            get { return _workingHoursPerDay; }
            set
            {
                if (_workingHoursPerDay == value) return;
                _workingHoursPerDay = value;
                _OnPropertyChange();
            }
        }

        private decimal _expectedCreatingOfCodeThreshold;
        public decimal ExpectedCreatingOfCodeThreshold
        {
            get { return _expectedCreatingOfCodeThreshold; }
            set
            {
                if (_expectedCreatingOfCodeThreshold == value) return;
                _expectedCreatingOfCodeThreshold = value;
                _OnPropertyChange();
            }
        }

        private decimal _expectedCreatingOfDocsThreshold;
        public decimal ExpectedCreatingOfDocsThreshold
        {
            get { return _expectedCreatingOfDocsThreshold; }
            set
            {
                if (_expectedCreatingOfDocsThreshold == value) return;
                _expectedCreatingOfDocsThreshold = value;
                _OnPropertyChange();
            }
        }

        private int _decimals = 2;
        public int Decimals
        {
            get { return _decimals; }
            set
            {
                if (_decimals == value)
                    return;
                _decimals = value;
                _OnPropertyChange();
            }
        }

        private decimal _dispersion;
        public decimal Dispersion
        {
            get { return _dispersion; }
            set
            {
                if (_dispersion == value) return;
                _dispersion = value;
                _OnPropertyChange();
            }
        }

        #region events

        public event PropertyChangedEventHandler PropertyChanged;

        public delegate void CalculationSettingsChangedDelegate(object sender, PropertyChangedEventArgs eventArgs);
        public event CalculationSettingsChangedDelegate SettingsChanged;

        private void _OnPropertyChange([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            SettingsChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        public static explicit operator CalculationSettings(CalculationSettingsConfigSection configModel)
        {
            return new CalculationSettings
            {
                WorkingHoursPerDay = configModel.WorkingHoursPerDay,
                ExpectedCreatingOfCodeThreshold = configModel.ExpectedCreatingOfCodeThreshold,
                ExpectedCreatingOfDocsThreshold = configModel.ExpectedCreatingOfDocsThreshold,
                Decimals = configModel.Decimals,
                Dispersion = configModel.Dispersion
            };
        }
    }
}

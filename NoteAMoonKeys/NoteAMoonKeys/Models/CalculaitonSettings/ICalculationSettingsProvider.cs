﻿namespace NoteAMoonKey.Models.CalculaitonSettings
{
    interface ICalculationSettingsProvider
    {
        CalculationSettings GetCalculationSettings { get; }

        void SaveCalculationSettings(CalculationSettings settingsToStore);
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using NoteAMoonKey.Models.CalculaitonSettings;

namespace NoteAMoonKey.Models
{
    public class ProcessingData : INotifyPropertyChanged
    {
        private IEnumerable<DateTime> _selectedDates;
        public IEnumerable<DateTime> SelectedDates
        {
            get { return _selectedDates; }
            set
            {
                if (_selectedDates == value)
                    return;

                _selectedDates = value;
                _OnPropertyContentChanged();
            }
        }

        private IEnumerable<NewWorkDayInfo> _resultCollection = new List<NewWorkDayInfo>();
        public IEnumerable<NewWorkDayInfo> ResultCollection
        {
            get {return _resultCollection;}
            set
            {
                if (_resultCollection?.Equals(value) ?? false)
                    return;

                _resultCollection = value;
                _OnPropertyContentChanged();
            }
        }

        private CalculationSettings _calculationSettings = new CalculationSettings();
        public CalculationSettings CalculationSettings
        {
            get { return _calculationSettings; }
            set
            {
                if (_calculationSettings.Equals(value))
                    return;
                
                _calculationSettings = value;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void _OnPropertyContentChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

﻿using System;

namespace NoteAMoonKey.Models
{
    public class NewWorkDayInfo
    {
        public DateTime Day { get; set; }
        public decimal CreationOfCodeHours { get; set; }
        public decimal CreationOfDocumentHours { get; set; }
        public decimal OtherTasksHours { get; set; }

        public decimal ExpectingCreativity { get; set; }

        public decimal Total => CreationOfCodeHours + CreationOfDocumentHours + OtherTasksHours;

        public override string ToString()
        {
            return $"Day: {Day:dd/MM/yyyy}; Creation: {CreationOfCodeHours}; Docs: {CreationOfDocumentHours}; Other: {OtherTasksHours}; Total: {Total}";
        }

        public void AdjustWith(NewWorkDayInfo newElement)
        {
            CreationOfCodeHours = newElement.CreationOfCodeHours;
            CreationOfDocumentHours = newElement.CreationOfDocumentHours;
            OtherTasksHours = newElement.OtherTasksHours;
        }

        public NewWorkDayInfo RestrictFractionalPartWith(int fractionalPartsLength)
        {
            CreationOfCodeHours = Math.Round(CreationOfCodeHours, fractionalPartsLength);
            CreationOfDocumentHours = Math.Round(CreationOfDocumentHours, fractionalPartsLength);
            OtherTasksHours = Math.Round(OtherTasksHours, fractionalPartsLength);

            return this;
        }
    }
}

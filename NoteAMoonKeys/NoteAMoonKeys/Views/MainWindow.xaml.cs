﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using NoteAMoonKey.Engines;
using NoteAMoonKey.Engines.WebPageSurfingEngine;
using NoteAMoonKey.Engines.WebPageSurfingEngine.SurfingEngineParts;
using NoteAMoonKey.Engines.WorkingHoursCalculator;
using NoteAMoonKey.Logger;
using NoteAMoonKey.Models;
using NoteAMoonKey.Resources.ConfigManager;
using Serilog;
using SimpleInjector;
using SimpleInjector.Lifestyles;
using Container = SimpleInjector.Container;
using System.Windows.Input;
using System.Windows.Media;
using OxyPlot;

namespace NoteAMoonKey.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            InitiateLogger();
            InitializeCalculationSettings();
        }
        
        private void InitializeCalculationSettings()
        {
            CalculationSettingsControl.InitializeWithConfig();
        }

        private void InitiateLogger()
        {
            LogDisplay.NotificationAction = message => Dispatcher.Invoke(() => {
                StatusTextBox.Text += "\n==>" + message;
                StatusTextBox.ScrollToEnd();
            });
        }

        private Container _InitializeSimpleInjectorContainer()
        {
            var container = new Container();
            container.Register<WebBrowser>(() => WebBrowserControl, Lifestyle.Singleton);
            container.Register<Dispatcher>(() => Dispatcher, Lifestyle.Singleton);
            container.Register<WebBrowserProcessor>(Lifestyle.Transient);
            container.Register<IWebPageSurfingEngine, WebPageSurfingEngine>(Lifestyle.Transient);
            container.Register<Trier>(Lifestyle.Transient);

            container.Register<IConfigManager>(() => App.container.GetInstance<IConfigManager>(), Lifestyle.Transient);

            container.Verify();

            return container;
        }

        private async void StartWorkButton_Click(object sender, RoutedEventArgs e)
        {
            CalculationSettingsControl.SaveParameters();

            using (var scope = ThreadScopedLifestyle.BeginScope(_InitializeSimpleInjectorContainer()))
            {
                var processingData = (ProcessingData) DataContext;

                if (processingData.SelectedDates == null || !processingData.SelectedDates.Any())
                {
                    MessageBox.Show(NoteAMoonKey.Resources.Notifications.SelectSomething);
                    return;
                }

                _SetControlElementsAvailability(false);
                _ShowWebBrowser();

                try
                {
                    var webPageSerfer = scope.Container.GetInstance<IWebPageSurfingEngine>();
                    await  webPageSerfer.AddWorkedDatesToTheSystem(processingData);
                }
                catch (Exception ex)
                {
                    Log.Error(ex, NoteAMoonKey.Resources.Notifications.SomethingWentWrong);
                    MessageBox.Show(NoteAMoonKey.Resources.Notifications.ProcessHasCrashed);
                }
                finally
                {
                    _SetControlElementsAvailability(true);
                }

                MessageBox.Show("Всё. Финиш!");
            }
        }

        private void _SetControlElementsAvailability(bool isEnabled)
        {
            WebBrowserControl.IsEnabled = isEnabled;
            Calendar.IsEnabled = isEnabled;
            CalculationSettingsControl.IsEnabled = isEnabled;
            WhatIsGoingOnButton.IsEnabled = isEnabled;
            ShowMeShowButton.IsEnabled = isEnabled;
        }

        private void WhatIsGoingOn_Click(object sender, RoutedEventArgs e)
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            _ShowWebBrowser(() => WebBrowserControl.Navigate($"file:///{currentDirectory}/About/index.html"));
        }

        private bool _IsWebBrowderShown { get; set; } = false;

        private void _ShowWebBrowser(Action navigationAction = null)
        {
            if (_IsWebBrowderShown)
            {
                navigationAction?.Invoke();
                return;
            }

            var showWebBrowserStoryboard = this.FindResource("ShowWebBrowserStoryboard") as Storyboard;

            if (showWebBrowserStoryboard == null)
                return;

            showWebBrowserStoryboard.Completed += (sender, args) =>
            {
                _IsWebBrowderShown = true;
                navigationAction?.Invoke();
            };

            showWebBrowserStoryboard.Begin();
        }

        private void Calendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            _RecalculateResultData();
        }

        private void CalculationSettingsControl_OnSettingsChanged(object sender, PropertyChangedEventArgs e)
        {
            _RecalculateResultData();
        }

        private void _RecalculateResultData()
        {
            var processingData = (ProcessingData)DataContext;
            processingData.SelectedDates = Calendar.SelectedDates.OrderBy(d => d);

            var workingHoursCalculator = App.container.GetInstance<IWorkingHoursCalculator>();

            try
            {
                processingData.ResultCollection = workingHoursCalculator.ProcessDates(processingData);
                NewWorkingDaysStatistics.Background = new SolidColorBrush(Color.FromRgb(255, 255, 255));
            }
            catch (NotPossibleCaseException ex)
            {
                Log.Information(ex, $"{NoteAMoonKey.Resources.Notifications.InappropriateSettings} {ex.CalculationSettings.Dispersion}");
                processingData.ResultCollection = new List<NewWorkDayInfo>();
                NewWorkingDaysStatistics.Background = new SolidColorBrush(Color.FromArgb(125, 247, 48, 48));
            }
            catch (Exception ex)
            {
                MessageBox.Show(NoteAMoonKey.Resources.Notifications.SomethingWentWrong);
            }
        }

        private void Calendar_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (Mouse.Captured is CalendarItem)
            {
                Mouse.Capture(null);
            }
        }
    }
}

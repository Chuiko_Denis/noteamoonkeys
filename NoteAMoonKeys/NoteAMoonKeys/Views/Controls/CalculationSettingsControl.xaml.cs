﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using NoteAMoonKey.Models.CalculaitonSettings;
using Serilog;
using Path = System.Windows.Shapes.Path;

namespace NoteAMoonKey.Views.Controls
{
    /// <summary>
    /// Interaction logic for AppSettingsControl.xaml
    /// </summary>
    public partial class CalculationSettingsControl : UserControl
    {
        public CalculationSettingsControl()
        {
            InitializeComponent();
            InitializeEventsBinding();
        }

        public void InitializeWithConfig()
        {
            var calculationSettingsManager = App.container.GetInstance<CalculationSettingsProvider>();
            DataContext = calculationSettingsManager.GetCalculationSettings;
        }

        private void InitializeEventsBinding()
        {
            DataContextChanged += CalculationSettingsControl_DataContextChanged;
        }

        private void CalculationSettingsControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var dataContext = (CalculationSettings)this.DataContext;
            dataContext.SettingsChanged += DataContext_SettingsChanged;
        }

        private void DataContext_SettingsChanged(object sender, PropertyChangedEventArgs e)
        {
            _OnSettingsChanged(e.PropertyName);
        }

        internal void SaveParameters()
        {
            try
            {
                var calculationSettingsManager = App.container.GetInstance<CalculationSettingsProvider>();
                calculationSettingsManager.SaveCalculationSettings((CalculationSettings) DataContext);
            }
            catch(Exception ex)
            {
                Log.Warning(ex, "An error occured during the [save calculation settings]-process.");
            }
        }

        #region events

        public delegate void CalculationSettingsChangedDelegate(object sender, PropertyChangedEventArgs e);

        public event CalculationSettingsChangedDelegate SettingsChanged;

        private void _OnSettingsChanged(string propertyName)
        {
            SettingsChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var thumb = (Thumb)DispersionSlider.Template.FindName("Thumb", DispersionSlider);
            if (thumb == null) return;

            var theGrip = (Path)thumb.Template.FindName("TheGrip", thumb);
            var fillBrush = (RadialGradientBrush)theGrip.Fill;


            var gradientStops = fillBrush.GradientStops.Select((gs, i) => i == 1 ? _AdjustGradientStop(gs, DispersionSlider) : gs);
            var newGradientBrush = new RadialGradientBrush(new GradientStopCollection(gradientStops));

            theGrip.Fill = newGradientBrush;
        }

        private GradientStop _AdjustGradientStop(GradientStop gradientStop, Slider slider)
        {
            var redColor = (byte)(255 * (slider.Value - slider.Minimum) / (slider.Maximum - slider.Minimum));

            var newColor = Color.FromArgb(gradientStop.Color.A, redColor, gradientStop.Color.G, gradientStop.Color.B);
            return new GradientStop(newColor, gradientStop.Offset);
        }
    }
}
